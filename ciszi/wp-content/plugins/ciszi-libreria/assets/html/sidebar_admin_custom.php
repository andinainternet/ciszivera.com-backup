<div class="ciszi-sidebar-admin">
	<?php if( $metafolders ) : ?>

	<input type="submit" class="btn btn-primary btn-block ciszi-admin-sfolder" value="Guardar y Salir" />
	<input type="submit" class="btn btn-default btn-block ciszi-admin-fremove" value="Borrar Folders" DISABLED />

	<ul class="ciszi-custom-admin">
	<?php foreach( $metafolders['list_folders'] as $kfolder => $vfolder ) : ?>
		<li>
			<input type="text" name="custom_folder_text[]" value="<?php echo $vfolder; ?>" class="text" />
			<input type="hidden" name="custom_folder_hidden[]" value="<?php echo $kfolder; ?>" />
			<span class="ciszi-list-check">
				<input type="checkbox" name="checkfold[]" value="<?php echo $kfolder; ?>" />
				<label class="ciszi-in-check" data-class="ciszi-custom-admin"></label>
			</span>
		</li>
	<?php endforeach; ?>
	</ul>

	<?php endif; ?>
</div>