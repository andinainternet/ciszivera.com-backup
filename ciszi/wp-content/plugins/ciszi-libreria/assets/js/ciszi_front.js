jQuery(function(){

	/*	Abrir popup	*/
	jQuery('body').on('click','.ciszi-saveto',function(event) {
		event.preventDefault();
		jQuery(this).next().slideToggle('fast');
	});

	/*	Abrir crear carpeta	*/
	jQuery('body').on('click','.ciszi-lnk-create-new',function(event) {
		event.preventDefault();
		jQuery(this).next().slideToggle('fast');
	});


	// Click en los checkbox
	jQuery('body').on('click','.ciszi-in-check',function() {

		console.log('click checkbox');

		if( jQuery(this).find('.ciszi-check-icon').length ) {

			jQuery(this).prev().removeAttr('checked');
			jQuery(this).find('.ciszi-check-icon').remove();
			
		} else {

			jQuery(this).prev().attr('checked','checked');
			jQuery(this).html('<span class="ciszi-check-icon glyphicon glyphicon-ok"></span>');
		}

		var postid = jQuery(this).closest('ul.ciszi-list-folders').attr('data-postid');
		var objcheck = jQuery('#ciszi-id-'+postid+' ul.ciszi-list-folders input[type=checkbox]:checked');

		if( objcheck.length == 0 )
			jQuery('#ciszi-id-'+postid+' .ciszi-front-assign').attr('disabled','disabled')
		else
			 jQuery('#ciszi-id-'+postid+' .ciszi-front-assign').removeAttr('disabled');

	});


	//Asignar post a carpetas, front
	jQuery('body').on('click','.ciszi-front-assign',function(event) {
		event.preventDefault();

		console.log('click asignar');
		
		jQuery(this).val('Guardando...');
		jQuery(this).attr('disabled','disabled');

		var i=0;
		var array_folders = Array();
		var postid = jQuery(this).attr('data-postid');

		jQuery('#ciszi-id-'+postid+' input[type=checkbox]:checked').each(function(index,element) {
			array_folders[i] = jQuery(this).val();
			i++;
		});

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_assign_post',
				idpost : postid,
				folders: array_folders,
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				jQuery('#ciszi-id-'+postid+' .ciszi-win-create-new').slideUp('fast');
				jQuery('#ciszi-id-'+postid+' .ciszi-popup').slideUp('fast');
				jQuery('#ciszi-id-'+postid).replaceWith(response);	
		});
	});


	// Creando una carpeta  vacía 
	jQuery('body').on('click','input[name=ciszi-btn-create-new]',function(event) {
		event.preventDefault();

		var postid = jQuery(this).attr('data-postid');
		var name_folder = jQuery('#ciszi-id-'+postid+' input[name=ciszi-in-create-new]').val();

		console.log(postid);

		jQuery(this).val('Creando...');
		jQuery(this).attr('disabled','disabled');

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_create_new',
				folder: name_folder,
				fromto : 'front',
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {

				console.log(response);

				jQuery('.ciszi-win-create-new').slideUp('fast');
				jQuery('#ciszi-id-'+postid+' input[name=ciszi-btn-create-new]').removeAttr('disabled');
				jQuery('#ciszi-id-'+postid+' input[name=ciszi-btn-create-new]').val('Crear');
				jQuery('ul.ciszi-list-folders').append(response);
		});
	});



	// Removiendo un post
	jQuery('body').on('click','.ciszi-saved',function(event) {
		event.preventDefault();
		var datapost = Array();

		var objciszi = jQuery(this);

		console.log('click removiendo un posts');
		
		datapost[0] = objciszi.attr('data-postid');
		objciszi.text('Removiendo...');	

		if(datapost.length == 0) return false;

		jQuery.post(
			CisziAjax.url,
			{
				action: 'ciszi_removing',
				datapost : datapost,
				fromto: 'front',
				wpnonce: CisziAjax.wpnonce
			},
			function(response) {
				objciszi.parent('.ciszi-to-library').replaceWith(response);
		});

	});
});