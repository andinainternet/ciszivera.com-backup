<?php

	/*
		PARA RECORDAR
		get_option('wc_memberships_rules');
	*/

	class ciszi {

		protected $settings;
		protected $defaults = array('postype' => 'post', 'ibootstrap' => 'yes');

		function __construct() {

			$this->settings = get_option('ciszi_settings',$this->defaults);

			$this->settings['postype'] = $this->settings['postype'] ? $this->settings['postype'] : $this->defaults['postype'];
			$this->settings['ibootstrap'] = $this->settings['ibootstrap'] ? $this->settings['ibootstrap'] : $this->defaults['ibootstrap'];


			//Agregamos el botón "Salvar en librería" en todos los post
			add_filter('the_content',array($this,'add_button_save'),20);

			// Settings
			add_action('plugins_loaded', array($this, 'addsettings'),20);

			//agregamos scripts
			add_action('wp_enqueue_scripts', array($this,'addscripts'),20 );

			//Shortocode
			add_shortcode('ciszi',array($this,'show_library'));

			//Ajax : crear folder nuevo
			add_action('wp_ajax_ciszi_create_new', array($this,'create_folder'));
			add_action('wp_ajax_nopriv_ciszi_create_new', array($this,'create_folder'));

			//Ajax : removiendo asignación
			add_action('wp_ajax_ciszi_removing', array($this,'removing_postid'));
			add_action('wp_ajax_nopriv_ciszi_removing', array($this,'removing_postid'));

			//Ajax: asignamos post a uno o varias carpetas
			add_action('wp_ajax_ciszi_assign_post', array($this,'assign_post'));
			add_action('wp_ajax_nopriv_ciszi_assign_post', array($this,'assign_post'));

			//Ajax: mover posts a diferentes folders
			add_action('wp_ajax_ciszi_move_posts', array($this,'move_posts'));
			add_action('wp_ajax_nopriv_ciszi_move_posts', array($this,'move_posts'));

			//Ajax: listar posts de un folder
			add_action('wp_ajax_ciszi_get_list', array($this,'list_post'));
			add_action('wp_ajax_nopriv_ciszi_get_list', array($this,'list_post'));	

			//Ajax: Administrar Folders
			add_action('wp_ajax_ciszi_admin_folders', array($this,'admin_folders'));		
			add_action('wp_ajax_nopriv_ciszi_admin_folders', array($this,'admin_folders'));

			//Ajax: Borrar Folders
			add_action('wp_ajax_ciszi_admin_fremove',array($this,'admin_fremove'));
			add_action('wp_ajax_ciszi_nopriv_admin_fremove',array($this,'admin_fremove'));
		}


		function addsettings() {
			if( current_user_can('administrator') ) {
				add_action('admin_menu', array($this,'add_plugin_page'));
				add_action('admin_init', array($this,'register_settings'));
			}
		}

		function add_plugin_page() {
			add_submenu_page('options-general.php', 'Library Ciszi', 'Library Ciszi', 'manage_options', 'ciszi', array($this,'add_submenu'));
		}


		function add_submenu() {
			include ( plugin_dir_path( __FILE__ ). 'assets/html/template_options.php' );
		}


		function register_settings() {

			register_setting(
            	'groupciszi', // Option group
            	'ciszi_settings', // Option name
            	array( $this, 'settings_sanitize' ) // Sanitize
        	);

        	add_settings_section(
            	'ciszi-seccion', // ID
            	'Opciones Libreria Ciszi', // Title
            	array( $this, 'print_section_info' ), // Callback
            	'settingciszi' // Page
        	);


        	add_settings_field(
            	'ciszi_posttype', // ID
            	'Seleccionar Post Type', // Title
            	array( $this, 'select_posttype' ), // Callback
            	'settingciszi', // Page
            	'ciszi-seccion' // Section
        	);

        	add_settings_field(
            	'ciszi_ibootstrap', // ID
            	'Incluir Bootstrap', // Title
            	array( $this, 'check_bootstrap' ), // Callback
            	'settingciszi', // Page
            	'ciszi-seccion' // Section
        	);
		}

		function print_section_info() {
			echo '<div>Opciones</div>';
		}

		function select_posttype() {

			$args = array();
			$listposts = get_post_types($args,'objects');

			echo '<select name="ciszi_settings[postype]">';

			foreach($listposts as $obj) {

				$selected = $this->settings['postype']  == $obj->name ? 'SELECTED' : '';
				echo '<option value="'.$obj->name.'" '.$selected.' >'.$obj->label.'</option>';
			}
			echo '</select>';
		}


		function check_bootstrap() {

			$checked_yes = $this->settings['ibootstrap'] == 'yes' ? 'CHECKED' : '';
			$checked_no = $this->settings['ibootstrap'] == 'no' ? 'CHECKED' : '';

			echo '<label><input type="radio" name="ciszi_settings[ibootstrap]" value="yes" '.$checked_yes.' />Si</label>';
			echo '<label><input type="radio" name="ciszi_settings[ibootstrap]" value="no" '.$checked_no.' />No</label>';

		}


		function addscripts() {

			if( $this->settings['ibootstrap'] == 'yes' )
				wp_enqueue_style( 'ciszi-boot', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );

			wp_enqueue_style( 'ciszi-css', plugins_url('assets/css/folders.css', __FILE__) );

			if( !is_page() ) {
				wp_enqueue_script('ciszi-js', plugins_url('assets/js/ciszi_front.js', __FILE__),array('jquery'),false,true);

				$array_ajax = array(
    					'url'		=> admin_url('admin-ajax.php'),
    					'wpnonce' 	=> wp_create_nonce( 'ciszi-wpnonce' )
    				);

				wp_localize_script('ciszi-js', 'CisziAjax', $array_ajax );
			}
		}


		function create_folder() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'ciszi-wpnonce' ) )
        		die ( 'Busted!');

        	$current_user = wp_get_current_user();
        	$userid = $current_user->ID;

			if( is_numeric($userid) ) {

				$fromto = $_POST['fromto'];
				$name_folder = $_POST['folder'];
				$key_folder = sanitize_title($_POST['folder']);

				$metafolders = get_user_meta($userid,'ciszi_organization',true);
				
				/*********INI:FOLDERS**********/
				if( isset($metafolders['list_folders']) ) { $i = 1;
					
					while( in_array($key_folder,array_keys($metafolders['list_folders'])) )
						$key_folder = $key_folder.'-'.$i++;	
				}
				
				$metafolders['list_folders'][$key_folder] = $name_folder;
				/*********FIN:FOLDERS**********/

				/*********INI:POSTS**********/
				$metafolders['details'][$key_folder] = array();
				/*********FIN:POSTS**********/

				update_user_meta($userid,'ciszi_organization',$metafolders);


				/*	Pa borrar	*/
				//$metafolders = get_user_meta($userid,'ciszi_organization',true);
				//update_option('itest1',print_r($metafolders,true));

				if( $fromto == 'admin' ) {
					//include( plugin_dir_path( __FILE__ ). 'assets/html/sidebar_admin_folders.php' );

					echo json_encode( array(
							'sidebar' => '<li><a href="" id="'.$key_folder.'" data-folder="'.$key_folder.'" class="ciszi-to-folder">'.$name_folder.'</a></li>',
							'popup' => '<li><span id="'.$key_folder.'" class="">'.$name_folder.'</span><span class="ciszi-list-check"><input type="checkbox" name="checkfold[]" value="'.$key_folder.'"><label class="ciszi-in-check" data-class="ciszi-list-folders"></label></span></li>'
						));
				} else
					echo '<li><span id="'.$key_folder.'" class="">'.$name_folder.'</span><span class="ciszi-list-check"><input type="checkbox" name="checkfold[]" value="'.$key_folder.'"><label class="ciszi-in-check" data-class="ciszi-list-folders"></label></span></li>';
			}

			die();
		}


		function removing_postid() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'ciszi-wpnonce' ) )
        		die ( 'Busted!');

			$current_user = wp_get_current_user();
			$userid = $current_user->ID;
			
			$array_postsids = $_POST['datapost'];
			$fromto = $_POST['fromto'];

			if( is_numeric($userid) && is_array($array_postsids) ) {

				$metafolders = get_user_meta($userid,'ciszi_organization',true);

				/*********INI:POSTS**********/
				foreach($array_postsids as $postid) {
					if( in_array($postid, $metafolders['list_posts']) ) {
					
						$keypost = array_search($postid, $metafolders['list_posts']);

						if( $keypost !== FALSE )
							unset($metafolders['list_posts'][$keypost]);

						// Buscamos el post dentro de los folders y lo borramos
						foreach($metafolders['details'] as $kfolder => $array_posts) {
							$detkeypost = array_search($postid, $array_posts);

							if( $detkeypost !== FALSE )
								unset($metafolders['details'][$kfolder][$detkeypost]);
						}
					}
				}
				/*********FIN:POSTS**********/

				update_user_meta($userid,'ciszi_organization',$metafolders);

				switch($fromto) {
					case 'front' : include( plugin_dir_path( __FILE__ ). 'assets/html/button_front_saveto.php' ); break;
					case 'admin' :
								$folder = $_POST['folder'];

								if( $folder == 'ciszi-all' )
									$array_posts = $metafolders['list_posts'];
								elseif( is_array($metafolders['details'][$folder]) && count($metafolders['details'][$folder])>0 )
				 					$array_posts = $metafolders['details'][$folder];
				 				else
				 					$array_posts = array(0);

								$args = array(
									'post_type' => $this->settings['postype'],
									'post__in' => $array_posts
								);
								$the_query = new WP_Query( $args );

								include( plugin_dir_path( __FILE__ ). 'assets/html/content_admin_folders.php' );
						break;
				}	
			}

			/*	Pa borrar	*/
			//$metafolders = get_user_meta($userid,'ciszi_organization',true);
			//update_option('itest1',print_r($metafolders,true));

			die();
		}

		
		function assign_post() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'ciszi-wpnonce' ) )
        		die ( 'Busted!');

        	$current_user = wp_get_current_user();

        	$userid = $current_user->ID;
			$postid = $_POST['idpost'];
			$array_folders = $_POST['folders'];

			if( is_numeric($userid) && is_numeric($postid) && is_array($array_folders) && count($array_folders)>0 ) {
				
				$metafolders = get_user_meta($userid,'ciszi_organization',true);

				/*********INI:POSTS**********/
				if( !in_array($postid, $metafolders['list_posts']) ) {
					$metafolders['list_posts'][] = $postid;

					foreach($array_folders as $key_folder)
						$metafolders['details'][$key_folder][] = $postid;
				}
				/*********FIN:POSTS**********/

				update_user_meta($userid,'ciszi_organization',$metafolders);

				include( plugin_dir_path( __FILE__ ). 'assets/html/button_front_saved.php' );

				/*	Pa borrar	*/
				//$metafolders = get_user_meta($userid,'ciszi_organization',true);
				//update_option('itest1',print_r($metafolders,true));
			}

			die();
		}


		function move_posts() {
			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'ciszi-wpnonce' ) )
        		die ( 'Busted!');

        	$current_user = wp_get_current_user();

        	$userid = $current_user->ID;
			$array_posts = $_POST['dataposts'];
			$array_folders = $_POST['datafolders'];

			if( is_array($array_posts) && count($array_posts)>0 &&
				is_array($array_folders) && count($array_folders)>0 ) {

				$metafolders = get_user_meta($userid,'ciszi_organization',true);

				/*********INI:POSTS**********/
				foreach($metafolders['details'] as $folder => $array_postid) {

					if( in_array($folder,$array_folders) ) { //asignamos
						update_option('htest3','entro1');
						$metafolders['details'][$folder] = array_unique(array_merge($metafolders['details'][$folder],$array_posts));
						update_option('htest4',$folder);
					} else { //borramos

						update_option('htest5','entro3');
						$metafolders['details'][$folder] = array_diff($metafolders['details'][$folder],$array_posts);
						update_option('htest6',$folder);

					}
				}
				/*********FIN:POSTS**********/

				update_user_meta($userid,'ciszi_organization',$metafolders);


				$args = array(
						'post_type' => $this->settings['postype'],
						'post__in' => $metafolders['list_posts'] ? $metafolders['list_posts'] : array(0)
					);
	
				$the_query = new WP_Query( $args );

				require_once plugin_dir_path( __FILE__ ).'assets/html/content_admin_folders.php';
			}

			die();
		}



		function list_post() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'ciszi-wpnonce' ) )
        		die ( 'Busted!');

        	$current_user = wp_get_current_user();

        	$userid = $current_user->ID;
			$folder = $_POST['folder'];

			if( !empty($folder) ) {
				$metafolders = get_user_meta($userid,'ciszi_organization',true);

				
				if( $folder != 'ciszi-all' ) {

					if( is_array($metafolders['details'][$folder]) && count($metafolders['details'][$folder])>0 )
						$array_posts = $metafolders['details'][$folder];
					else
						$array_posts = array(0);
				 } else {

				 	if( is_array($metafolders['list_posts']) && count($metafolders['list_posts'])>0 )
				 		$array_posts = $metafolders['list_posts'];
				 	else
				 		$array_posts = array(0);
				 }


				$args = array(
						'post_type' => $this->settings['postype'],
						'post__in' => $array_posts
					);
	
				$the_query = new WP_Query( $args );

				require_once plugin_dir_path( __FILE__ ).'assets/html/content_admin_folders.php';
			}

			die();
		}



		function add_button_save($content) {
			ob_start();

			global $post;

			//if( is_user_logged_in() && in_array($post->post_type,$this->settings['post-type']) ) {
			if( is_user_logged_in() && $post->post_type == $this->settings['postype'] && !is_page() ) {
				
				$current_user = wp_get_current_user();
				$metafolders = get_user_meta($current_user->ID,'ciszi_organization',true);
				$postid = $post->ID;

				if( isset($metafolders['list_posts']) ) {
					if( in_array($post->ID, $metafolders['list_posts']) )
						include( plugin_dir_path( __FILE__ ). 'assets/html/button_front_saved.php' );
					else
						include( plugin_dir_path( __FILE__ ). 'assets/html/button_front_saveto.php' );
				} else
					include( plugin_dir_path( __FILE__ ). 'assets/html/button_front_saveto.php' );				
			}

			echo $content;

			$output = ob_get_contents();
    		ob_end_clean();
    	
    		return $output;
		}


		function admin_folders() {

			$current_user = wp_get_current_user();
			$metafolders = get_user_meta($current_user->ID,'ciszi_organization',true);

			$array_vfolders = $_POST['vfolders'];
			$array_kfolders = $_POST['kfolders'];

			update_option('ptest1',print_r($_POST,true));

			if( isset($array_vfolders) && isset($array_kfolders) ) {

				foreach($array_kfolders as $key => $kfolder)
					$metafolders['list_folders'][$kfolder] = $array_vfolders[$key];

				update_user_meta($current_user->ID,'ciszi_organization',$metafolders);

				include_once plugin_dir_path( __FILE__ ).'assets/html/sidebar_admin_folders.php';

			} else
				include_once plugin_dir_path( __FILE__ ).'assets/html/sidebar_admin_custom.php';

			die();
		}


		function admin_fremove() {

			if ( ! wp_verify_nonce( $_POST['wpnonce'], 'ciszi-wpnonce' ) )
        		die ( 'Busted!');

			$array_folders = $_POST['folders'];
			$array_admin_posts = array();
			$current_user = wp_get_current_user();
			$metafolders = get_user_meta($current_user->ID,'ciszi_organization',true);

			if( is_array($array_folders) && count($array_folders) > 0 ) {

				foreach($array_folders as $kfolder) {

					if( isset($metafolders['list_folders'][$kfolder]) )
						unset($metafolders['list_folders'][$kfolder]);

					if( isset($metafolders['details'][$kfolder]) )
						unset($metafolders['details'][$kfolder]);
				}


				foreach($metafolders['details'] as $kfolder => $array_posts)
					$array_admin_posts = array_unique(array_merge($array_admin_posts,$array_posts));

				unset($metafolders['list_posts']);
				$metafolders['list_posts'] = $array_admin_posts;

				update_user_meta($current_user->ID,'ciszi_organization',$metafolders);
			}
			
			include_once plugin_dir_path( __FILE__ ).'assets/html/sidebar_admin_custom.php';
			die();
		}


		function show_library() {

			ob_start();

			$array_ajax = array(
   					'url'		=> admin_url('admin-ajax.php'),
   					'wpnonce' 	=> wp_create_nonce( 'ciszi-wpnonce' )
   				);

			wp_enqueue_script('ciszi-ad', plugins_url('assets/js/ciszi_admin.js', __FILE__),array('jquery'),false,true);
			wp_localize_script('ciszi-ad', 'CisziAjax', $array_ajax );

			$current_user = wp_get_current_user();
			$metafolders = get_user_meta($current_user->ID,'ciszi_organization',true);

			if( is_user_logged_in() ) {

				if( isset($metafolders['list_posts']) && count($metafolders['list_posts'])>0 )
					$array_posts = $metafolders['list_posts'];
				else
					$array_posts = array(0);

				$args = array(
						'post_type' => $this->settings['postype'],
						'post__in' => $array_posts
					);
	
				$the_query = new WP_Query( $args );

				require_once plugin_dir_path( __FILE__ ).'assets/html/shortcode.php';
				
			} else
				echo 'No esta conectado';


			$output = ob_get_contents();
    		ob_end_clean();
    	
    		return $output;
		}

	}
?>