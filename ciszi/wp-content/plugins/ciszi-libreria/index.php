<?php
/*
Plugin Name: Library Ciszi
Plugin URI: https://www.letsgodev.com/
Description: Pemite crear una biblioteca con los post del WP
Version: 1.0.0
Author: GoPymes SAC
Author URI: http://www.gopymes.pe
Developer: GoPymes SAC
Developer URI: https://www.letsgodev.com/
Requires at least: 4.7.1
Tested up to: 4.7
Stable tag: 4.7
*/

//ciszi member son los usuarios que pagan


/*	Add custom settings url in plugins page	*/
function ciszi_setting_internal_link( $links ) {
	$settings = array(
					'settings' => sprintf('<a href="%s">%s</a>', admin_url( 'options-general.php?page=ciszi'), 'Configurar')
				);
	
	return array_merge( $settings, $links );
}
add_filter( 'plugin_action_links_'.plugin_basename( __FILE__ ), 'ciszi_setting_internal_link' );


require_once plugin_dir_path( __FILE__ ).'/ciszi.class.php';
$ciszi = new ciszi();

?>