
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Consultas
 *
 * @package ciszi-theme
 */
get_header(); ?>
<link rel="stylesheet" href="../wp-content/themes/ciszi-theme/lib/fullpage/jquery.fullpage.css"/>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <section>
      <div id="fullpage" class="consultas-sm-txt col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <section id="section1" class="section section1">
          <div id="slide1" class="slide 1">
            <div class="consulting-col consulting-main_txt col-lg-12">
              <article class="consulting-txt consulting-txt-hero">
                <h3>¿Quieres Profundizar<br/>en tu Viaje personal?</h3>
                <div class="consulting-txt-separator"></div>
                <h1>Una consulta personal</h1>
                <p class="consulting-txt-parrf">es tomar la decisión de ir a la acción<br/>y acelerar tu proceso</p>
                <div class="consulting-txt-img"><img src="../wp-content/themes/ciszi-theme/assets/images/brand-blue.png" class="consulting-txt_logo"/></div>
              </article>
            </div>
          </div>
        </section>
        <section id="section2" class="section section2">
          <div class="consulting-col-secondary consulting-main_txt col-lg-12">
            <article class="consulting-txt"><span class="consulting-txt-span consulting-term-main">
                <?php do_action( 'storefront_loop_before' );
while ( have_posts() ) : the_post();
	get_template_part( 'content', get_post_format() );
endwhile;
do_action( 'storefront_loop_after' ); ?></span></article>
          </div>
        </section>
        <section id="section3" class="section section3">
          <div class="consulting-col-secondary consulting-main_txt col-lg-12">
            <article class="consulting-txt">
              <div class="consulting-txt-img">
              </div><span class="consulting-term-main"><?php the_field(consulta_seccion_herramientas); ?></span>
            </article>
          </div>
        </section>
        <section id="section4" class="section section4">
          <div class="consulting-col-secondary consulting-main_txt col-lg-12">
            <article class="consulting-txt">
              <div class="consulting-txt-img">
              </div><span class="consulting-term-main"><?php the_field(consulta_seccion_condiciones); ?></span>
            </article>
          </div>
        </section>
        <section id="section5" class="section section5">
          <div class="consulting-col-secondary consulting-main_txt col-lg-12">
            <article class="consulting-txt consulting-txt-form">
              <div class="consulting-txt-img">
              </div><span class="consulting-term-main consulting-form"><?php the_field(consulta_seccion_formulario); ?></span>
            </article>
          </div>
        </section>
      </div>
      <div id="col_consultas" class="consultas-sm-bg col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="consulting-col-secondary2 col-lg-12">
          <section id="myCarousel" class="carousel slide carousel-fade">
            <div class="carousel-inner">
              <div class="item active">
                <div class="fill"></div>
                <div class="carousel-caption"><img src="<?php the_field(consultas_imagen_1); ?>" ALT="Ciszi" class="consulting-img1"/></div>
              </div>
              <div class="item">
                <div class="fill"></div>
                <div class="carousel-caption"><img src="<?php the_field(consultas_imagen_2); ?>" ALT="Ciszi" class="consulting-img2"/></div>
              </div>
              <div class="item">
                <div class="fill"></div>
                <div class="carousel-caption"><img src="<?php the_field(consultas_imagen_3); ?>" ALT="Ciszi" class="consulting-img3"/>
                </div>
              </div>
            </div>
          </section>
          <div class="central_bar_blue"></div>
        </div>
      </div>
    </section>
  </main>
</div>
<script src="../wp-content/themes/ciszi-theme/lib/js/carousel-home.js"></script>
<?php get_footer(); ?>