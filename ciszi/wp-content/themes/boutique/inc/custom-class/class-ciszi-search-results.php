<?php 

//=================================
// CISZI SEARCH RESULTS
//=================================
if ( ! function_exists( 'storefront_post_content_excerpt' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	
	function storefront_post_content_excerpt() {
		?>
		<?php

		/**
		 * Functions hooked in to storefront_post_content_before action.
		 *
		 * @hooked storefront_post_thumbnail - 10
		 */
		

		// get_the_excerpt();
		$tamano = 400;
    	$longitud_titulo = 35;
    	$longitud_desc = 200;
		$bla = 'bla';
		$excerpt = get_the_excerpt();
		
		$salida = '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'"><p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p></a>';
		
			echo $salida


		
		?>
		<?php

		do_action( 'storefront_post_content_after' );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
			'after'  => '</div>',
		) );
		?>
		<?php
	}
}

if ( ! function_exists( 'storefront_post_header_search' ) ) {
	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_post_header_search() {
		?>
		
		<?php
		if ( is_single() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} else {
			if ( 'post' == get_post_type() ) {
			}

			the_title( sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' );
		}
		?>
		
		<?php
	}
}

if ( ! function_exists( 'storefront_post_content_post_format' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	
	function storefront_post_content_post_format() {
		?>
		
		<?php 
			$post_format = '<div class="bar-formatPost bar-formatPost-'. get_post_format($format, $post_id) . '"></div>';
 		?>
		<div class="bar-format-container">
			<div class="bar-format-container">
				<?php echo $post_format ?>
				<div class="bar-formatPost-buttonSave"></div>
			</div>

		<?php

		

		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
			'after'  => '</div>',
		) );
		?>
		</div><!-- .entry-content -->
		<?php
	}
}


 ?>