
<?php 

//=================================
//CUSTOM FIELDS FOR REGISTER FORM
//=================================

// output the form field
add_action('register_form', 'ad_register_fields');
	function ad_register_fields() {
	?>
	    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide register-custom-input-name">
	        <label for="firstname"><?php _e('Nombre') ?><br />
	        <input type="firstname" name="firstname" id="firstname" class="woocommerce-Input woocommerce-Input--text input-text" value="<?php echo esc_attr($_POST['firstname']); ?>" size="100" tabindex="20" />
	        </label>
	    </p>
	<?php
}

// save new first name
add_filter('pre_user_first_name', 'ad_user_firstname');
	function ad_user_firstname($firstname) {
	    if (isset($_POST['firstname'])) {
	        $firstname = $_POST['firstname'];
	    }
	    return $firstname;
	}

	 ?>