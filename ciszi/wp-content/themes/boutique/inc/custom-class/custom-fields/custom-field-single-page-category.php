<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_single-page-category',
		'title' => 'Single Page Category',
		'fields' => array (
			array (
				'key' => 'field_58efa26472803',
				'label' => 'Imagen Principal',
				'name' => 'single_page_img_principal_desktop',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_590a54217177d',
				'label' => 'Imagen Principal Mobile',
				'name' => 'single_page_img_principal_mobile',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-single-category.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-member-zone.php',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


 ?>