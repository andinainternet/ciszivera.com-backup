<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_consultas',
		'title' => 'Consultas',
		'fields' => array (
			array (
				'key' => 'field_59147f92b0c9a',
				'label' => 'Contenido',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_58d3f10b5030e',
				'label' => 'Herramientas',
				'name' => 'consulta_seccion_herramientas',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de las herramientas',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f1175030f',
				'label' => 'Condiciones de Servicio',
				'name' => 'consulta_seccion_condiciones',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de las condiciones de Servicio',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f12b50311',
				'label' => 'Formulario de Contacto',
				'name' => 'consulta_seccion_formulario',
				'type' => 'wysiwyg',
				'instructions' => 'Aquí se coloca el "shortcode" del Formulario de contacto',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59147fa0b0c9b',
				'label' => 'Imagenes',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_59147fc7b0c9c',
				'label' => 'Imagen 1',
				'name' => 'consultas_imagen_1',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59147fd9b0c9d',
				'label' => 'Imagen 2',
				'name' => 'consultas_imagen_2',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59147fe6b0c9e',
				'label' => 'Imagen 3',
				'name' => 'consultas_imagen_3',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '87',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}



 ?>