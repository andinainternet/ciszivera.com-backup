
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Single Page Style 2
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <div class="video-bg">
      <section id="video_bg" class="single-page-bg">
        <div class="single-page-main"><img src="<?php the_field(single_page_img_principal); ?>" alt="Video" class="single-page-item-img hidden-xs"/><img src="../wp-content/themes/ciszi-theme/assets/images/video_movil.png" alt="Video" class="single-page-item-img hidden-lg hidden-md hidden-sm"/></div>
      </section>
    </div>
    <div class="video-content">
      <section id="singlePage2_content">
        <div class="single-page-2-main col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="single-page-2-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php do_action( 'storefront_loop_before' );
while ( have_posts() ) : the_post();
	get_template_part( 'content', get_post_format() );
endwhile;
do_action( 'storefront_loop_after' ); ?>
          </div>
        </div>
      </section>
    </div>
  </main>
</div>
<?php get_footer(); ?>