<?php
/**
 * Template used to display post content.
 *
 * @package storefront
 */

?>
	<article id="post-<?php the_ID(); ?>" class="card-main-item col-blog col-blog-2 col-lg-12 col-md-12 col-sm-12 col-xs-12" <?php post_class(); ?>>
		<div class="card-ctn-body" >
			<div class="card-item-img">
				<?php 
					/*
					 * Functions hooked in to storefront_loop_post action.
					 *
					 * @hooked storefront_post_thumbnail          - 10
					 *
					*/
					do_action('storefront_loop_results_img')
				?>
			</div>
			
			<div class="posts_content card-item-txt card-item-txt-off">
				<?php
				/**
				 * Functions hooked in to storefront_loop_post action.
				 *
				 * @hooked storefront_post_header            - 20
				 * @hooked storefront_post_content_excerpt         - 30
				 */
					
					do_action( 'storefront_loop_results_cnt' );
				?>
			</div>

			<div class="card-ctn-title">
				<?php do_action('storefront_loop_results_title'); ?>
			</div>

		<?php do_action('storefront_loop_results_format'); ?>



		</div>

	</article><!-- #post-## -->

