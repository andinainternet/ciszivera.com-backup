<?php
/**
 * Boutique engine room
 *
 * @package boutique
 */

/**
 * Set the theme version number as a global variable
 */
$theme				= wp_get_theme( 'boutique' );
$boutique_version	= $theme['Version'];

$theme				= wp_get_theme( 'storefront' );
$storefront_version	= $theme['Version'];

/**
 * Load the individual classes required by this theme
 */
require_once( 'inc/class-boutique.php' );
require_once( 'inc/class-boutique-customizer.php' );
require_once( 'inc/class-boutique-template.php' );
require_once( 'inc/class-boutique-integrations.php' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/**
 * Do not add custom code / snippets here.
 * While Child Themes are generally recommended for customisations, in this case it is not
 * wise. Modifying this file means that your changes will be lost when an automatic update
 * of this theme is performed. Instead, add your customisations to a plugin such as
 * https://github.com/woothemes/theme-customisations
 */

/*ADD CUSTOM GOOGLE FONTS*/

// function theme_add_theme_font() {

// 	wp_enqueue_script( 'google-fonts','https://fonts.googleapis.com/css?family=Raleway:300,400' );
// }

// add_action( 'wp_enqueue_scripts', 'theme_add_theme_font' );

// <link href="https://fonts.googleapis.com/css?family=Raleway:300,400" rel="stylesheet">


//ADD CUSTOM LOGO WP-ADMIN

function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/assets/images/logo_ciszi_vera_wp.png) !important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

//CUSTOM DESTINY SHOP

add_filter( 'woocommerce_return_to_shop_redirect', 'return_to_shop_link' );
  
function return_to_shop_link() {
    //capturamos la ID de la página original y su traducción con la fx wpml_object_id
    $shop_id = apply_filters( 'wpml_object_id', 57, 'page' );
    //obtenemos la URL de la página con la ID anterior
    $shop_url = get_permalink( $shop_id );
  
     return $shop_url; // change to the link you want
}
  



/*ADD JQUERY*/
function theme_add_jquery() {

	wp_enqueue_script( 'jquery-js','https://code.jquery.com/jquery-3.1.1.js', array(), '3.1.1', false );
}

add_action( 'wp_enqueue_scripts', 'theme_add_jquery' );



// ADD BOOTSTRAP
function theme_add_bootstrap() {

wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/lib/bootstrap/css/bootstrap.min.css' );
// wp_enqueue_style( 'app-css', get_template_directory_uri() . '/lib/app.css' );
wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/lib/bootstrap/js/bootstrap.min.js', array(), '3.0.0', true );

}

add_action( 'wp_enqueue_scripts', 'theme_add_bootstrap' );


//ADD POST TYPES
function theme_support() {

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'video', 'quote',  'gallery', 'audio',
	) );

	add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );	

}

add_action( 'after_setup_theme', 'theme_support' );


//ADD SCRIPT FULLPAGESCROLL
function theme_page_scroll () {
	wp_enqueue_style('page_scroll_css','https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.css');
	wp_enqueue_script('page_scroll_js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.js');
	
}

add_action( 'wp_enqueue_scripts', 'theme_page_scroll' );

//ADD SCRIPT CUSTOM JS
function theme_custom_js () {
	wp_enqueue_script('custom_js', get_template_directory_uri() . '/lib/js/app.js');
	
}

add_action( 'wp_enqueue_scripts', 'theme_custom_js' );


// ADD CUSTOM SIDEBAR BOUTIQUE
function sidebar_boutique(){
	register_sidebar(array(
		"name" => "Boutique Sidebar",
		"id" => "btq_sidebar",
		"descripcion" => "Sidebar en Boutique",
		"class" => "btqSidebar",
		"before_widget" => "",
		"after_widget" => "",
		"before_title" => "<h2 class='titulodelwidget'>",
		"after_title" => "</h2>"
	));
}
add_action('widgets_init','sidebar_boutique');


// ADD CUSTOM SIDEBAR LIBRARY
function sidebar_libreria(){
	register_sidebar(array(
		"name" => "Libreria Sidebar",
		"id" => "btq_libreria",
		"descripcion" => "Sidebar en Libreria",
		"class" => "libSidebar",
		"before_widget" => "",
		"after_widget" => "",
		"before_title" => "<h3 style='font-size: 14px;' class='titulodelwidget'>Compártelo",
		"after_title" => "</h3>"
	));
}
add_action('widgets_init','sidebar_libreria');

// ADD CUSTOM SIDEBAR LIBRARY CATEGORY BAR
function siderbar_category_bar(){
	register_sidebar(array(
		"name" => "Category Bar",
		"id" => "lib_category_bar",
		"descripcion" => "Categorias de los Posts en la Libreria",
		"class" => "lib_catbar",
		// "before_widget" => "<li id='%1$s' class='%2$s'>",
		"before_widget" => "",
		"after_widget" => "",
		"before_title" => "<h2 class='titulodelwidget'>",
		"after_title" => "</h2>"
	));
}
add_action('widgets_init','siderbar_category_bar');



//=====================================
// ADD CUSTOM DEFAULT POST TO CATEGORIES
//=====================================
function shortcode_post_categories($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => '',
		'longitud_titulo' => 100,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350,
		'cat' => '' 
	), $atts));
 
	$query = array(	'showposts' => $limite,  
					'orderby'=> 'date', 
					'order'=>'DESC', 
					'post_status' => 
					'publish', 
					'ignore_sticky_posts' => 1, 
					'cat' => $cat

					);
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
		/*SECTION - IMAGE POST*/
		$salida .= '<div class="card-ctn-body">';
			$salida .= '<div class="card-item-img">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
				$salida .= '</a>';
		$salida .= '</div>';
		

		/*SECTION - TITLEPOST*/
		$salida .= '<div class="card-ctn-title">';
			$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
			$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo, ' ...' ).'';
			$salida .= '</a>';
 		
 		// $salida .= '<div class="container-barFormat">';
		// $salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
			$salida .= '<div class="bar-format-container">
						<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';
		// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>				
		// $salida .= '</div>';		

		$salida .= '</div>';
		
		/*SECTION - DESCRIPTION POST*/
		$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
		 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				// $salida .= '<h3>'.get_the_title().'</h3>';

		 		/* Escribo extracto  */
			$excerpt = get_the_excerpt();
			$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';
		 	$salida .= '</a>';
 		$salida .= '</div>';




	
		endif;


	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('post_categories', 'shortcode_post_categories');


//=====================================
// ADD CUSTOM DEFAULT POST TO "VIDEO"
//=====================================

function shortcode_custom_content($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => 24,
		'longitud_titulo' => 100,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350,
		'post_format' => '',
		'tag' => ''
 
	), $atts));
 
	$query = array(
    				'post_format' => $post_format,
    				'tag' => $tag
    				);
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
		/*SECTION - IMAGE POST*/
		$salida .= '<div class="card-ctn-body">';
			$salida .= '<div class="card-item-img">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
				$salida .= '</a>';
		$salida .= '</div>';
		

		/*SECTION - TITLEPOST*/
		$salida .= '<div class="card-ctn-title">';
			$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
			$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo, ' ...' ).'';
			$salida .= '</a>';
 		
 		// $salida .= '<div class="container-barFormat">';
		// $salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
			$salida .= '<div class="bar-format-container">
						<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';
		// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>				
		// $salida .= '</div>';		

		$salida .= '</div>';
		
			/*SECTION - DESCRIPTION POST*/
		$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
		 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				// $salida .= '<h3>'.get_the_title().'</h3>';

		 		/* Escribo extracto  */
			$excerpt = get_the_excerpt();
			$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';
		 	$salida .= '</a>';
 		$salida .= '</div>';

	
		endif;


	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('custom-content', 'shortcode_custom_content');


//MEMBERS ZONE POST
function shortcode_members($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => 5,
		'longitud_titulo' => 20,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350
 
	), $atts));
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
		/*SECTION - IMAGE POST*/
		$salida .= '<div class="card-ctn-body">';
			$salida .= '<div class="card-item-img">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
				$salida .= '</a>';
		$salida .= '</div>';
		

		/*SECTION - TITLEPOST*/
		$salida .= '<div class="card-ctn-title">';
			$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
			$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo, ' ...' ).'';
			$salida .= '</a>';
 		
 		// $salida .= '<div class="container-barFormat">';
		// $salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
			$salida .= '<div class="bar-format-container">
						<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';
		// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>				
		// $salida .= '</div>';		

		$salida .= '</div>';
		
		/*SECTION - DESCRIPTION POST*/
		$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
		 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				// $salida .= '<h3>'.get_the_title().'</h3>';

		 		/* Escribo extracto  */
			$excerpt = get_the_excerpt();
			$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';
		 	$salida .= '</a>';
 		$salida .= '</div>';
	
		endif;


	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('members', 'shortcode_members');



//ADD SVG

add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = array() ) {
	// Add the file extension to the array
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}

//SKIP CROPPEP


//* Add support for custom flexible header
add_theme_support( 'custom-header', array(
	'flex-width'    => true,
	'width'           => 127,
	'flex-height'    => true,
	'height'          => 110,
	'header-selector' => '.site-branding .custom-logo-link',
	'header-text'     => false
 
) );

//FRONTEND UPLOADES

function my_fu_after_upload( $attachment_ids, $success, $post_id ) {
	// do something with freshly uploaded files
	// This happens on POST request, so $_POST will also be available for you
	$redirecturl = 'google.com';
	wp_redirect( 'http://' .  $redirecturl );
	exit;
}


//WOOCOMMERCE PASSWORD STRONG


add_action ('wp_print_scripts', function () {
	if (wp_script_is ('wc-password-strength-meter', 'enqueued'))
		wp_dequeue_script ('wc-password-strength-meter');
}, 100);




//=================================
//CUSTOM FIELDS FOR REGISTER FORM
//=================================

// output the form field
add_action('register_form', 'ad_register_fields');
	function ad_register_fields() {
	?>
	    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide register-custom-input-name">
	        <label for="firstname"><?php _e('Nombre') ?><br />
	        <input type="firstname" name="firstname" id="firstname" class="woocommerce-Input woocommerce-Input--text input-text" value="<?php echo esc_attr($_POST['firstname']); ?>" size="100" tabindex="20" />
	        </label>
	    </p>
	<?php
}

// save new first name
add_filter('pre_user_first_name', 'ad_user_firstname');
	function ad_user_firstname($firstname) {
	    if (isset($_POST['firstname'])) {
	        $firstname = $_POST['firstname'];
	    }
	    return $firstname;
	}

//=================================
// SHOW AND PRINT CATEGORIES
//=================================
function wpb_catlist_desc() { 
// $attachment = wp_get_attachment_image_src( $images[$cat_id], $size );
$categories = wp_get_post_categories( $object_id );

$string = '<div id="style-1" class="lib-subcat-main">';

$catlist = get_terms( 'category' );

if ( ! empty( $catlist ) ) {
  foreach ( $catlist as $key => $item ) {

	  	$img_id = $item->term_id;
		$images = cfi_featured_image_url( array( 'cat_id' => $img_id ) );
		$cat_ft_title = cfi_featured_image( array( 'title' => '36' ) );
		$cat_title = 'http://test.tupino.com/ciszi/'. $cat_title;
		$cat_id = intval( $args['cat_id'] );
		$cat_img = get_the_category($image);


	    $string .= '<div class="lib-subcat-item">
						<div class="lib-item-img-main">
							<a href="'. $item->slug .'"><img src="'. $images .'" /></a>
						</div>
						<div class="lib-item-txt-main item-txt-main-hide">
							<div class="lib-item-txt-title">
								<h5>' . $item->name . '</h5>
				    			<i class="lib-item-txt-icon"></i>
							</div>
							<div class="lib-item-txt-descrp">
			    				<p>'. $item->description . '</p>
			    			</div>
	    				</div>
	    			</div>';
	    			}
}
$string .= '</div>';

return $string; 
}
add_shortcode('wpb_categories', 'wpb_catlist_desc');



//=================================
// CISZI SEARCH RESULTS
//=================================
include('inc/custom-class/class-ciszi-search-results.php');


//=================================
//CUSTOM FIELDS
//=================================
include('inc/custom-class/ciszi-custom-fields.php');