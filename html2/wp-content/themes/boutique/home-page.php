
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Homepage
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <section id="sectionHeroHome">
      <div class="heroMain">
        <div class="hero-text col-lg-7 col-md-7 col-sm-6 col-xs-12">
          <article>
            <div class="hero-separator hero-txt-item"></div>
            <p class="hero-txt-item">Deseas la transformación.</p>
            <h2 class="hero-txt-item">Revelar tu Potencial.</h2>
            <h2 class="hero-txt-item">Tu poder.</h2>
            <h1 class="hero-txt-item">¿Qué deseas revelar <span>hoy</span>?</h1>
            <div class="hero-txt-item hero-separator"></div>
          </article>
        </div>
        <div class="hero-img col-lg-5 col-md-5 col-sm-6 col-xs-12">
          <div style="background-image: url('./wp-content/themes/ciszi-theme/assets/images/car-1.png');" class="hero-img-itembk"></div>
        </div>
      </div>
    </section>
    <section id="sectionWords">
      <div class="word-main">
        <div class="words-border">
          <article>
            <p>Mi compromiso es compartir recursos <br/> que te recuerden quien eres, entrega al mundo tu</p>
            <section id="myCarousel" class="carousel slide">
              <div class="carousel-inner">
                <div class="item active">
                  <div class="fill"></div>
                  <div class="carousel-caption">
                    <h2 class="home-slider-txt">TALENTO</h2>
                  </div>
                </div>
                <div class="item">
                  <div class="fill"></div>
                  <div class="carousel-caption">
                    <h2 class="home-slider-txt">PASIÓN</h2>
                  </div>
                </div>
                <div class="item">
                  <div class="fill"></div>
                  <div class="carousel-caption">
                    <h2 class="home-slider-txt">AMOR</h2>
                  </div>
                </div>
                <div class="item">
                  <div class="fill"></div>
                  <div class="carousel-caption">
                    <h2 class="home-slider-txt">CREATIVIDAD</h2>
                  </div>
                </div>
                <div class="item">
                  <div class="fill"></div>
                  <div class="carousel-caption">
                    <h2 class="home-slider-txt">AUTENTICIDAD</h2>
                  </div>
                </div>
              </div>
              <div class="subtitle-border"></div>
            </section>
          </article>
        </div>
      </div>
    </section>
    <section id="sectionServices">
      <div class="serv-main">
        <div class="serv-row row">
          <div class="serv-item serv-item-1 col-lg-3 col-md-3 col-sm-12 col-xs-12"><a href="<?php the_field('destino_1'); ?>">
              <div class="serv-item-bg-1 serv-item-bg-mob-1"></div>
              <div class="serv-item-layout serv-item-layout-1 serv-layout-on"></div>
              <div class="serv-item-txt serv-item-txt-1 serv-item-txt-up">
                <h2><?php the_field('servicio_1_titulo'); ?></h2>
                <p><?php the_field('servicio_1_descripcion'); ?></p>
              </div></a></div>
          <div class="serv-item serv-item-2 col-lg-3 col-md-3 col-sm-12 col-xs-12"><a href="<?php the_field('destino_2'); ?>">
              <div class="serv-item-bg-2 serv-item-bg-mob-2"></div>
              <div class="serv-item-layout serv-item-layout-2 serv-layout-on"></div>
              <div class="serv-item-txt serv-item-txt-2 serv-item-txt-up">
                <h2><?php the_field('servicio_2_titulo'); ?></h2>
                <p><?php the_field('servicio_2_descripcion'); ?></p>
              </div></a></div>
          <div class="serv-item serv-item-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"><a href="<?php the_field('destino_3'); ?>">
              <div class="serv-item-bg-3 serv-item-bg-mob-3"></div>
              <div class="serv-item-layout serv-item-layout-3 serv-layout-on"></div>
              <div class="serv-item-txt serv-item-txt-3 serv-item-txt-up">
                <h2><?php the_field('servicio_3_titulo'); ?></h2>
                <p><?php the_field('servicio_3_descripcion'); ?></p>
              </div></a></div>
          <div class="serv-item serv-item-4 col-lg-3 col-md-3 col-sm-12 col-xs-12"><a href="<?php the_field('destino_4'); ?>">
              <div class="serv-item-bg-4 serv-item-bg-mob-4"></div>
              <div class="serv-item-layout serv-item-layout-4 serv-layout-on"></div>
              <div class="serv-item-txt serv-item-txt-4 serv-item-txt-up">
                <h2><?php the_field('servicio_4_titulo'); ?></h2>
                <p><?php the_field('servicio_4_descripcion'); ?></p>
              </div></a></div>
        </div>
      </div>
    </section>
    <section id="sectionPhilosofy">
      <div class="philo-main row">
        <div class="philo-item">
          <h2 class="philo-item-txt">Conoce mi <span class="home-philo-item-txt2"><a href="/mi-filosofia" class="philo-item-txt-a">Filosofia</a></span></h2>
        </div>
      </div>
    </section>
  </main>
</div>
<script src="./wp-content/themes/ciszi-theme/lib/js/carousel-home.js"></script>
<?php get_footer(); ?>