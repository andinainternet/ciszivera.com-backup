
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Member
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <section id="cisziMember-page">
      <section id="content-member" class="row">
        <?php do_action( 'storefront_loop_before' );
while ( have_posts() ) : the_post();
	get_template_part( 'content', get_post_format() );
endwhile;
do_action( 'storefront_loop_after' ); ?>
      </section>
      <section id="memberships-member" class="row">
        <div class="memberships-plans-main">
          <h3 class="membership-subtitle"><span>Planes</span></h3>
          <?php $args = array(
				'post_type' => 'planes', 
				'posts_per_page' => 6, 
				'order' => 'asc'); ?>
          <?php $the_query = new WP_Query( $args ); ?>
          <?php if ( $the_query->have_posts() ) {; ?>
          <?php while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
          <div class="memberships-plans-container row">
            <div class="memberships-plans-item">
              <div class="memberships-plans-header">
                <div class="memberships-plans-header-container">
                  <h4 class="memberships-h5">Plan</h4>
                  <h5><?php the_title(); ?></h5>
                </div>
                <p><?php the_content(); ?></p>
              </div>
              <div class="memberships-plans-body col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div style="background-image: url('<?php the_field(planbg_img); ?>')" class="planBg_img memberships-plans-body-item col-lg-6 col-md-6 col-sm-6 col-xs-12"><a href="<?php home_url(); ?>/finalizar-compra/?add-to-cart=<?php the_field(link_plan_1); ?>">
                    <figure><?php the_post_thumbnail('medium'); ?></figure></a>
                  <!-- h5 COMPRAR-->
                </div>
                <div class="memberships-plans-body-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <p><?php the_excerpt(); ?></p>
                </div>
              </div>
            </div>
          </div>
          <?php }; ?>
          <?php }; ?>
        </div>
      </section>
    </section>
  </main>
</div>
<?php get_footer(); ?>