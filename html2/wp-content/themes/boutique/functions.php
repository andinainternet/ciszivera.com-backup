<?php
/**
 * Boutique engine room
 *
 * @package boutique
 */

/**
 * Set the theme version number as a global variable
 */
$theme				= wp_get_theme( 'boutique' );
$boutique_version	= $theme['Version'];

$theme				= wp_get_theme( 'storefront' );
$storefront_version	= $theme['Version'];

/**
 * Load the individual classes required by this theme
 */
require_once( 'inc/class-boutique.php' );
require_once( 'inc/class-boutique-customizer.php' );
require_once( 'inc/class-boutique-template.php' );
require_once( 'inc/class-boutique-integrations.php' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/**
 * Do not add custom code / snippets here.
 * While Child Themes are generally recommended for customisations, in this case it is not
 * wise. Modifying this file means that your changes will be lost when an automatic update
 * of this theme is performed. Instead, add your customisations to a plugin such as
 * https://github.com/woothemes/theme-customisations
 */

/*ADD CUSTOM GOOGLE FONTS*/

// function theme_add_theme_font() {

// 	wp_enqueue_script( 'google-fonts','https://fonts.googleapis.com/css?family=Raleway:300,400' );
// }

// add_action( 'wp_enqueue_scripts', 'theme_add_theme_font' );

// <link href="https://fonts.googleapis.com/css?family=Raleway:300,400" rel="stylesheet">


//ADD CUSTOM LOGO WP-ADMIN

function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/assets/images/logo_ciszi_vera_wp.png) !important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

//CUSTOM DESTINY SHOP

add_filter( 'woocommerce_return_to_shop_redirect', 'return_to_shop_link' );
  
function return_to_shop_link() {
    //capturamos la ID de la página original y su traducción con la fx wpml_object_id
    $shop_id = apply_filters( 'wpml_object_id', 57, 'page' );
    //obtenemos la URL de la página con la ID anterior
    $shop_url = get_permalink( $shop_id );
  
     return $shop_url; // change to the link you want
}
  



/*ADD JQUERY*/
function theme_add_jquery() {

	wp_enqueue_script( 'jquery-js','https://code.jquery.com/jquery-3.1.1.js', array(), '3.1.1', false );
}

add_action( 'wp_enqueue_scripts', 'theme_add_jquery' );



// ADD BOOTSTRAP
function theme_add_bootstrap() {

wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/lib/bootstrap/css/bootstrap.min.css' );
// wp_enqueue_style( 'app-css', get_template_directory_uri() . '/lib/app.css' );
wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/lib/bootstrap/js/bootstrap.min.js', array(), '3.0.0', true );

}

add_action( 'wp_enqueue_scripts', 'theme_add_bootstrap' );


//ADD POST TYPES
function theme_support() {

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'video', 'quote',  'gallery', 'audio',
	) );

	add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );	

}

add_action( 'after_setup_theme', 'theme_support' );


//ADD SCRIPT FULLPAGESCROLL

function theme_page_scroll () {
	wp_enqueue_style('page_scroll_css','https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.css');
	wp_enqueue_script('page_scroll_js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.js');
	
}

add_action( 'wp_enqueue_scripts', 'theme_page_scroll' );

//ADD SCRIPT CUSTOM JS

function theme_custom_js () {
	wp_enqueue_script('custom_js', get_template_directory_uri() . '/lib/js/app.js');
	
}

add_action( 'wp_enqueue_scripts', 'theme_custom_js' );


// ADD CUSTOM SIDEBAR BOUTIQUE

function sidebar_boutique(){
	register_sidebar(array(
		"name" => "Boutique Sidebar",
		"id" => "btq_sidebar",
		"descripcion" => "Sidebar en Boutique",
		"class" => "btqSidebar",
		"before_widget" => "",
		"after_widget" => "",
		"before_title" => "<h2 class='titulodelwidget'>",
		"after_title" => "</h2>"
	));
}
add_action('widgets_init','sidebar_boutique');


// ADD CUSTOM SIDEBAR LIBRARY

function sidebar_libreria(){
	register_sidebar(array(
		"name" => "Libreria Sidebar",
		"id" => "btq_libreria",
		"descripcion" => "Sidebar en Libreria",
		"class" => "libSidebar",
		"before_widget" => "",
		"after_widget" => "",
		"before_title" => "<h3 style='font-size: 14px;' class='titulodelwidget'>Compártelo",
		"after_title" => "</h3>"
	));
}
add_action('widgets_init','sidebar_libreria');

// ADD CUSTOM SIDEBAR LIBRARY CATEGORY BAR

function siderbar_category_bar(){
	register_sidebar(array(
		"name" => "Category Bar",
		"id" => "lib_category_bar",
		"descripcion" => "Categorias de los Posts en la Libreria",
		"class" => "lib_catbar",
		// "before_widget" => "<li id='%1$s' class='%2$s'>",
		"before_widget" => "",
		"after_widget" => "",
		"before_title" => "<h2 class='titulodelwidget'>",
		"after_title" => "</h2>"
	));
}
add_action('widgets_init','siderbar_category_bar');

//POST

function shortcode_recientes($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => 5,
		'longitud_titulo' => 100,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350
 
	), $atts));
 
	$query = array('showposts' => $limite,  'orderby'=> 'date', 'order'=>'DESC', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
		/*SECTION - IMAGE POST*/
		$salida .= '<div class="card-ctn-body">';
			$salida .= '<div class="card-item-img">';
				$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
				$salida .= '</a>';
		$salida .= '</div>';
		

		/*SECTION - TITLEPOST*/
		$salida .= '<div class="card-ctn-title">';
			$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
			$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo, ' ...' ).'';
			$salida .= '</a>';
 		
 		// $salida .= '<div class="container-barFormat">';
		// $salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
			$salida .= '<div class="bar-format-container">
						<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';
		// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'"><span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria</a>				
		// $salida .= '</div>';		

		$salida .= '</div>';
		
		/*SECTION - DESCRIPTION POST*/
		$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
		 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
				$salida .= '<h3>';
				/* Calculo las categorías  */
				$categories = get_the_category();
				$separator = ' ';
				$output = '';
				if($categories){
						foreach($categories as $category) {
								$output .= $category->cat_name.''.$separator;
						}
						$salida .= trim($output, $separator);
				}
			$salida .= '</h3>';
		 		/* Escribo extracto  */
			$excerpt = get_the_excerpt();
			$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';
		 	$salida .= '</a>';
 		$salida .= '</div>';




	
		endif;


	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('recientes', 'shortcode_recientes');


//VIDEO POST

function shortcode_video($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => 5,
		'longitud_titulo' => 100,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350
 
	), $atts));
 
	$query = array(
    'post_format' => 'post-format-video');
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
	$salida .= '<div class="card-ctn-title">';
	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
	$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo );
	$salida .= '</a>';
	$salida .= '</div>';

	$salida .= '<div class="card-ctn-body">';
	
	$salida .= '<div class="card-item-img">';
	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
	$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
	$salida .= '</div>';
	$salida .= '</a>';
	
	endif;
	$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';

	$salida .= '<h3>';
 
		/* Calculo las categorías  */
 
		$categories = get_the_category();
		$separator = ' ';
		$output = '';
		if($categories){
				foreach($categories as $category) {
						$output .= $category->cat_name.''.$separator;
				}
				$salida .= trim($output, $separator);
		}
	$salida .= '</h3>';

 		/* Escribo extracto  */
 
	$excerpt = get_the_excerpt();
	$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';

 	$salida .= '</a>';
 	$salida .= '</div>';
	


	$salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
	$salida .= '<div class="bar-format-container">
					<div class="bar-formatPost"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';
							// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'">
							// 	<span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria
							// </a>
	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('video-post', 'shortcode_video');


//MEMBERS ZONE POST

function shortcode_members($atts, $content = null, $code) {
 
//Uso: [recientes  limite="3" longitud_titulo="50" longitud_desc="50" thumbnail="1" tamano="50"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
	extract(shortcode_atts(array(
		'limite' => 5,
		'longitud_titulo' => 20,
		'longitud_desc' => 450,
		'thumbnail' => false,
		'tamano' => 350
 
	), $atts));
 
	$query = array(
    'post_format' => 'post-format-video');
 
	$q = new WP_Query($query);
	if ($q->have_posts()) :
	$salida  = '';
	$salida .= '<div class="listado-videos-recientes col-lg-12 col-md-12 col-sm-12 col-xs-12">';
 
	/* comienzo while */
	while ($q->have_posts()) : $q->the_post();
	
	$salida .= '<div class="card-main-item col-lg-4 col-md-4 col-sm-6 col-xs-12">';
	if ( has_post_thumbnail() && $thumbnail == true):
	
	$salida .= '<div class="card-ctn-title">';
	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
	$salida .= wp_html_excerpt (get_the_title(), $longitud_titulo );
	$salida .= '</a>';
	$salida .= '</div>';

	$salida .= '<div class="card-ctn-body">';
	
	$salida .= '<div class="card-item-img">';
	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';
	$salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));
	$salida .= '</div>';
	$salida .= '</a>';
	
	endif;
	$salida .= '<div class="posts_content card-item-txt card-item-txt-off">';
 	$salida .= '<a href="'.get_permalink().'" title="'.sprintf( "Enlace permanente a %s", get_the_title() ).'">';

	$salida .= '<h3>';
 
		/* Calculo las categorías  */
 
		$categories = get_the_category();
		$separator = ' ';
		$output = '';
		if($categories){
				foreach($categories as $category) {
						$output .= $category->cat_name.''.$separator;
				}
				$salida .= trim($output, $separator);
		}
	$salida .= '</h3>';

 		/* Escribo extracto  */
 
	$excerpt = get_the_excerpt();
	$salida .= ($excerpt)?'<p>'.wp_html_excerpt($excerpt,$longitud_desc).'</p>':'';

 	$salida .= '</a>';
 	$salida .= '</div>';
	


	$salida .= '<div class="bar-formatPost bar-formatPost-'.get_post_format($format, $post_id).'"></div>';
	$salida .= '<div class="bar-format-container">
					<div class="bar-formatPost"></div>
						<div class="bar-formatPost-buttonSave">
						</div>
					</div>';

							// <a href="" class="ciszi-collection ciszi-saveto" data-postid="'.$post_id.'">
							// 	<span class="glyphicon glyphicon-plus-sign"></span> Guardar en tu libreria
							// </a>

	$salida .= '</div>';
	$salida .= '</div>';
	endwhile;
	wp_reset_query();
	/* fin while */
 
	$salida .= '</div>';
	endif;
 
	return $salida;
 
}

add_shortcode('members', 'shortcode_members');



//ADD SVG

add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = array() ) {
	// Add the file extension to the array
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}

//SKIP CROPPEP


//* Add support for custom flexible header
add_theme_support( 'custom-header', array(
	'flex-width'    => true,
	'width'           => 127,
	'flex-height'    => true,
	'height'          => 110,
	'header-selector' => '.site-branding .custom-logo-link',
	'header-text'     => false
 
) );

//FRONTEND UPLOADES

function my_fu_after_upload( $attachment_ids, $success, $post_id ) {
	// do something with freshly uploaded files
	// This happens on POST request, so $_POST will also be available for you
	$redirecturl = 'google.com';
	wp_redirect( 'http://' .  $redirecturl );
	exit;
}


// SHOW AND PRINT CATEGORIES

function wpb_catlist_desc() { 
// $attachment = wp_get_attachment_image_src( $images[$cat_id], $size );
$categories = wp_get_post_categories( $object_id );




$string = '<div id="style-1" class="lib-subcat-main">';


$catlist = get_terms( 'category' );

if ( ! empty( $catlist ) ) {
  foreach ( $catlist as $key => $item ) {

	  	$img_id = $item->term_id;
		$images = cfi_featured_image_url( array( 'cat_id' => $img_id ) );
		$cat_ft_title = cfi_featured_image( array( 'title' => '36' ) );
		$cat_title = 'http://test.tupino.com/ciszi/'. $cat_title;
		$cat_id = intval( $args['cat_id'] );
		$cat_img = get_the_category($image);


	    $string .= '<div class="lib-subcat-item">
						<div class="lib-item-img-main">
							<a href="'. $item->slug .'"><img src="'. $images .'" /></a>
						</div>
						<div class="lib-item-txt-main item-txt-main-hide">
							<div class="lib-item-txt-title">
								<h5>' . $item->name . '</h5>
				    			<i class="lib-item-txt-icon"></i>
							</div>
							<div class="lib-item-txt-descrp">
			    				<p>'. $item->description . '</p>
			    			</div>
	    				</div>
	    			</div>';
	    			}
}
$string .= '</div>';

return $string; 
}
add_shortcode('wpb_categories', 'wpb_catlist_desc');

//=================================
//CUSTOM FIELDS
//=================================

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_bg-single-page',
		'title' => 'BG Single Page',
		'fields' => array (
			array (
				'key' => 'field_58efa26472803',
				'label' => 'Imagen Principal',
				'name' => 'single_page_img_principal',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_58f53275729f5',
				'label' => 'Título',
				'name' => 'single_page_title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58f53288729f6',
				'label' => 'Sub Título ',
				'name' => 'single_page_subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58f532b1729f7',
				'label' => 'Single Page Ico',
				'name' => 'single_page_ico',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-single-2.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-boutique.php',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_boutique-a',
		'title' => 'Boutique A',
		'fields' => array (
			array (
				'key' => 'field_59038e79fd1cc',
				'label' => 'Imagen 1 A',
				'name' => 'btq_imagen_1_a',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-boutique.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_boutique-b',
		'title' => 'Boutique B',
		'fields' => array (
			array (
				'key' => 'field_58f414fb09de1',
				'label' => 'Imagen 1 B',
				'name' => 'btq_imagen_1_b',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_58f413d9c9dd9',
				'label' => 'Link Seccción 1 B',
				'name' => 'btq_link_secccion_1_b',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58f4150409de2',
				'label' => 'Imagen 2 B',
				'name' => 'btq_imagen_2_b',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_58f413e3c9ddb',
				'label' => 'Link Seccción 2 B',
				'name' => 'btq_link_secccion_2_b',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58f4150c09de3',
				'label' => 'Imagen 3 B',
				'name' => 'btq_imagen_3_b',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_58f413dfc9dda',
				'label' => 'Link Seccción 3',
				'name' => 'btq_link_secccion_3',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-boutique.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_boutique-background',
		'title' => 'Boutique Background',
		'fields' => array (
			array (
				'key' => 'field_59037b1419960',
				'label' => 'Background Boutique',
				'name' => 'background_boutique',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-boutique.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_boutique-c',
		'title' => 'Boutique C',
		'fields' => array (
			array (
				'key' => 'field_59038db1affca',
				'label' => 'Imagen 1 C',
				'name' => 'btq_imagen_1_c',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59038e01affcd',
				'label' => 'Link Sección 1 C',
				'name' => 'btq_link_secccion_1_c',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_59038dd1affcb',
				'label' => 'Imagen 2 C',
				'name' => 'btq_imagen_2_c',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59038e21affce',
				'label' => 'Link Sección 2 C',
				'name' => 'link_seccion_2_c',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_59038defaffcc',
				'label' => 'Imagen 3 C',
				'name' => 'btq_imagen_3_c',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59038e2faffcf',
				'label' => 'Link Sección 3',
				'name' => 'link_seccion_3_c',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-boutique.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_ciszi-membrer',
		'title' => 'Ciszi Membrer',
		'fields' => array (
			array (
				'key' => 'field_58d32a3ec6404',
				'label' => 'Link Plan de Membreria',
				'name' => 'link_plan_1',
				'type' => 'relationship',
				'instructions' => 'Registra el Plan de Membresía que corresponde',
				'return_format' => 'id',
				'post_type' => array (
					0 => 'product',
				),
				'taxonomy' => array (
					0 => 'product_cat:18',
				),
				'filters' => array (
					0 => 'search',
				),
				'result_elements' => array (
					0 => 'post_type',
					1 => 'post_title',
				),
				'max' => '',
			),
			array (
				'key' => 'field_58e67bc493da7',
				'label' => 'Bakground Image Plan',
				'name' => 'planbg_img',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'planes',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_consultas',
		'title' => 'Consultas',
		'fields' => array (
			array (
				'key' => 'field_58d3f0ac5030c',
				'label' => 'Consulta Sección 1',
				'name' => 'consulta_seccion_1',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de la sección 2. Aqui la primera letra es "Capital"',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f0fc5030d',
				'label' => 'Consulta Sección 2',
				'name' => 'consulta_seccion_2',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de la sección 3',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f10b5030e',
				'label' => 'Consulta Sección 3',
				'name' => 'consulta_seccion_3',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de la sección 3',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f1175030f',
				'label' => 'Consulta Sección 4',
				'name' => 'consulta_seccion_4',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de la sección 4',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f11f50310',
				'label' => 'Consulta Sección 5',
				'name' => 'consulta_seccion_5',
				'type' => 'wysiwyg',
				'instructions' => 'Descripción de la sección 5',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d3f12b50311',
				'label' => 'Consulta Sección 6',
				'name' => 'consulta_seccion_6',
				'type' => 'wysiwyg',
				'instructions' => 'Aquí se coloca el "shortcode" del Formulario de contacto',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '87',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_libreria',
		'title' => 'Libreria',
		'fields' => array (
			array (
				'key' => 'field_58d18ff8e71d1',
				'label' => 'Hero Text',
				'name' => 'hero_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-libreria.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_philosofy',
		'title' => 'Philosofy',
		'fields' => array (
			array (
				'key' => 'field_58d2b45c18200',
				'label' => 'Mi Filosofia',
				'name' => 'mi_filosofia',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d404644a28a',
				'label' => 'Acerca de Mi',
				'name' => 'acerca_de_mi',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_58d2b49918202',
				'label' => 'Biografía',
				'name' => 'biografia',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-page-filosofia.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_servicio-1',
		'title' => 'Servicio 1',
		'fields' => array (
			array (
				'key' => 'field_58d3415514543',
				'label' => 'Titulo',
				'name' => 'servicio_1_titulo',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3416614544',
				'label' => 'Descripción',
				'name' => 'servicio_1_descripcion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d341b614545',
				'label' => 'Destino',
				'name' => 'destino_1',
				'type' => 'page_link',
				'post_type' => array (
					0 => 'page',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '8',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_servicio-2',
		'title' => 'Servicio 2',
		'fields' => array (
			array (
				'key' => 'field_58d3434a27bb2',
				'label' => 'Titulo',
				'name' => 'servicio_2_titulo',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3435e27bb3',
				'label' => 'Descripción',
				'name' => 'servicio_2_descripcion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3437827bb4',
				'label' => 'Destino',
				'name' => 'destino_2',
				'type' => 'page_link',
				'post_type' => array (
					0 => 'page',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '8',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_servicio-3',
		'title' => 'Servicio 3',
		'fields' => array (
			array (
				'key' => 'field_58d3c4f18c91f',
				'label' => 'Titulo',
				'name' => 'servicio_3_titulo',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3c4fc8c920',
				'label' => 'Descripción',
				'name' => 'servicio_3_descripcion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3c5118c921',
				'label' => 'Destino',
				'name' => 'destino_3',
				'type' => 'page_link',
				'post_type' => array (
					0 => 'page',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '8',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_servicio-4',
		'title' => 'Servicio 4',
		'fields' => array (
			array (
				'key' => 'field_58d3c546c9823',
				'label' => 'Titulo',
				'name' => 'servicio_4_titulo',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3c558c9824',
				'label' => 'Descripción',
				'name' => 'servicio_4_descripcion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d3c56dc9825',
				'label' => 'Destino',
				'name' => 'destino_4',
				'type' => 'page_link',
				'post_type' => array (
					0 => 'page',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '8',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
