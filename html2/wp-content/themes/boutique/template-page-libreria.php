
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Libreria
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <section id="libreria-subcategorias" class="row">
      <?php echo do_shortcode("[wpb_categories]"); ?>
    </section>
    <section id="libreria-hero" class="libreria-hero-main row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 libreria-hero-img">
        <div style="background-image: url('../wp-content/themes/ciszi-theme/assets/images/caro-lib.png');" class="hero-img-main hero-img-main-bg">
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 libreria-hero-txt">
        <div class="hero-txt-main">
          <h2>Libreria</h2>
          <p> <?php the_field('hero_text'); ?></p>
        </div>
      </div>
    </section>
    <section id="libreria-posts" class="row">
      <div class="libreria-category-bar row">
        <?php do_action( 'storefront_loop_before' );
while ( have_posts() ) : the_post();
	get_template_part( 'content', get_post_format() );
endwhile;
do_action( 'storefront_loop_after' ); ?>
      </div>
      <div class="libreria-posts"></div>
    </section>
  </main>
</div>
<?php get_footer(); ?>