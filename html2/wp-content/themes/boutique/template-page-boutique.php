
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Boutique
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main btqMain">
    <div class="btq-bg col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <section id="boutique_bg" class="single-page-bg">
        <div class="single-page-main"><img src="<?php the_field(background_boutique); ?>" alt="Video" class="single-page-item-img hidden-xs"/><img src="../wp-content/themes/ciszi-theme/assets/images/btq_movil.png" alt="Video" class="single-page-item-img hidden-lg hidden-md hidden-sm"/></div>
      </section>
    </div>
    <div class="boutique-content">
      <div class="btq-sidebar col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <div id="btq_sidebar" class="btqSidebar">
          <?php if ( is_active_sidebar('btq_sidebar') ) {
dynamic_sidebar('btq_sidebar');
}; ?>
        </div>
      </div>
      <div class="btq-section col-lg-10 col-md-10 col-sm-10 col-xs-12">
        <section id="sectionBoutique">
          <div class="btq-main col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="boutique-main-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="btq-col btq-col-1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="btq-col-item col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(btq_link_secccion_1_a); ?>"><img src="<?php the_field(btq_imagen_1_a); ?>"/></a></div>
              </div>
            </div>
            <div class="boutique-main-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="btq-col btq-col-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="btq-col-item col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(btq_link_secccion_1_b); ?>"><img src="<?php the_field(btq_imagen_1_b); ?>"/></a>
                </div>
              </div>
              <div class="btq-col btq-col-1 col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="btq-col-item col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(btq_link_secccion_2_b); ?>"><img src="<?php the_field(btq_imagen_2_b); ?>"/></a>
                </div>
                <div class="btq-col-item col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(btq_link_secccion_3_b); ?>"><img src="<?php the_field(btq_imagen_3_b); ?>"/></a>
                </div>
              </div>
            </div>
            <div class="boutique-main-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="btq-col btq-col-1 col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="btq-col-item col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(tq_link_secccion_1_c); ?>"><img src="<?php the_field(btq_imagen_1_c); ?>"/></a>
                </div>
                <div class="btq-col-item col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(btq_link_secccion_2_c); ?>"><img src="<?php the_field(btq_imagen_2_c); ?>"/></a>
                </div>
              </div>
              <div class="btq-col btq-col-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="btq-col-item col-lg-13 col-md-12 col-sm-12 col-xs-12"><a href="<?php the_field(btq_link_secccion_3_c); ?>"><img src="<?php the_field(btq_imagen_3_c); ?>"/></a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="sectionBoutique2">
        </section>
      </div>
    </div>
  </main>
</div>
<?php get_footer(); ?>