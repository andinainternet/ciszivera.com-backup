
<?php /**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: CISZI Mi Libreria
 *
 * @package ciszi-theme
 */
get_header(); ?>
<div id="primary" class="conten-area">
  <main id="main" role="main" class="site-main">
    <div class="container-miLibreria">
      <div id="section-header">
        <div class="cisziFolder-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="cisziFolder-title">
            <h3>MI LIBRERIA </h3>
          </div>
          <?php global $current_user; get_currentuserinfo(); ?>
          <?php if (is_user_logged_in()){
$cisziFolder_msgFull = '<div class="cisziFolder-msgFull"><h2>Bienvenida ' . $current_user->user_login . '</h2></div>'; 
	echo $cisziFolder_msgFull;} 
	else {
		$cisziFolder_msgEmpty = '<div class="cisziFolder-container"><div class="cisziFolder-msgEmpty"><h2>Bienvenida</h2></div><div class="cisziFolder-msgEmpty-p"><p>Guardar el contenido con el que haz conectado en tu libreria en carpetas bajo el nombre que tu quieras. Una manera de tener lo que necesitas a tu alcance, no hay mas excusas para no hacer el trabajo.</p></div></div>';
		$cisziFolder_btnSingup = '<div class="btn-signup-mylib col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="btn-singup-cnt col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="btn-signup-item col-lg-2 col-lg-offset-5 col-md-2 col-lg-offset-5 col-sm-4 col-sm-offset-4 col-xs-12" href="../mi-cuenta">REGISTRATE</a></div></div>';
			echo $cisziFolder_msgEmpty;
			echo $cisziFolder_btnSingup;}; ?>
        </div>
      </div>
      <div id="section-cisziFolder">
        <div class="cisziFolder-folders col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?php echo do_shortcode('[ciszi]'); ?>
        </div>
      </div>
    </div>
  </main>
</div>
<?php get_footer(); ?>