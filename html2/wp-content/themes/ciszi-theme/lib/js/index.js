jQuery(document).ready(function() {

	let url 			=	window.location.host;
	let brandUrl		=	'../wp-content/themes/ciszi-theme/assets/images/logo_verde.svg';
	let brandLogo 		=	brandUrl;
	let socialBar 		= 	'<div class="socialBar socialBar-top socialBar-bottom"></div>';
	let icoig			=	'<div class="socialBar-item"><a href="#" class="fa fa-instagram"></a></div>';
	let icofb			=	'<div class="socialBar-item"><a href="#" class="fa fa-facebook"></a></div>';
	let icotw			=	'<div class="socialBar-item"><a href="#" class="fa fa-twitter"></a></div>';
	let icopn			=	'<div class="socialBar-item"><a href="#" class="fa fa-pinterest-p"></a></div>';	
	let siteSearch		=	$('.site-search');
	let searchPlaceHolder = $('#woocommerce-product-search-field').attr('placeholder');
	let searchDisable	= 'placeholder-search-field-disable';
	let searchActive	= 'placeholder-search-field-active';
	
	$('#socialBar_footer').append(icoig, icofb, icopn, icotw);

	$('.site-search').hover(function(){
		$(this).toggleClass('activebox');
	});

	$('#woocommerce-product-search-field').attr('placeholder', 'Buscador');
	$('#woocommerce-product-search-field').addClass(searchDisable);

	$('#woocommerce-product-search-field').hover(function(){
		$('#woocommerce-product-search-field').toggleClass(searchDisable);
		$('#woocommerce-product-search-field').toggleClass(searchActive);	
	});
	
	$('#footer_brandLogo').attr('src', brandLogo);

	// HOME : FUNCTION ABOUT ANIMATION SERVICES

	$('.serv-item-layout-1').hover(function() {
		$('.serv-item-layout-1').toggleClass('serv-layout-on');
		$('.serv-item-layout-1').toggleClass('serv-layout-off');
		$('.serv-item-txt-1').toggleClass('serv-item-txt-up');
		$('.serv-item-txt-1').toggleClass('serv-item-txt-down');
	})
	$('.serv-item-layout-2').hover(function() {
		$('.serv-item-layout-2').toggleClass('serv-layout-on');
		$('.serv-item-layout-2').toggleClass('serv-layout-off');
		$('.serv-item-txt-2').toggleClass('serv-item-txt-up');
		$('.serv-item-txt-2').toggleClass('serv-item-txt-down');		
	})
	$('.serv-item-layout-3').hover(function() {
		$('.serv-item-layout-3').toggleClass('serv-layout-on');
		$('.serv-item-layout-3').toggleClass('serv-layout-off');
		$('.serv-item-txt-3').toggleClass('serv-item-txt-up');
		$('.serv-item-txt-3').toggleClass('serv-item-txt-down');
	})	
	$('.serv-item-layout-4').hover(function() {
		$('.serv-item-layout-4').toggleClass('serv-layout-on');
		$('.serv-item-layout-4').toggleClass('serv-layout-off');
		$('.serv-item-txt-4').toggleClass('serv-item-txt-down');
	})

	// LIBRERIA : FUNCTION ABOUT ANIMATION LIBRARY

	$('.lib-item-txt-main').hover(function () {
		$(this).toggleClass('item-txt-main-show');
		$(this).toggleClass('item-txt-main-hide');
	});

	$('#item_lib2').hover(function () {
		$('#item_lib2').toggleClass('item-txt-main-show');
		$('#item_lib2').toggleClass('item-txt-main-hide');
	});
	
	$('#item_lib3').hover(function () {
		$('#item_lib3').toggleClass('item-txt-main-show');
		$('#item_lib3').toggleClass('item-txt-main-hide');
	});	
	
	$('#item_lib4').hover(function () {
		$('#item_lib4').toggleClass('item-txt-main-show');
		$('#item_lib4').toggleClass('item-txt-main-hide');
	});

	$('#item_lib5').hover(function () {
		$('#item_lib5').toggleClass('item-txt-main-show');
		$('#item_lib5').toggleClass('item-txt-main-hide');
	});

	$('input[type=radio]').attr('checked', 'checked');


	$(".usearchbtn").click(function() {
		console.log('funciona');
		$("#contenedorajax").removeClass('ajaxHeight');		
	});
	
	$('.bar-postFormat-audio').addClass('fa', 'fa-volume-down');
	$('.bar-postFormat-video').addClass('fa', 'fa-video-camera');

	let btnAjax				= $('.usearchbtn');
	let btnAjax_class_on	= 'usearchbtn-on';
	
	let btnAjax_observer	= 'btnAjax-observer';
	let btnAjax_estilo		= 'usfbtn-activeBar';
	
	let defaultActive 		= $('#uwpqsffrom_185 > #uwpqsf_btn > .usearchbtn');

	$(defaultActive).addClass('usfbtn-activeBar');

	$(btnAjax).addClass(btnAjax_observer);

	$(btnAjax).click(function(e){
		$(btnAjax).removeClass(btnAjax_estilo);
	
		$(this).addClass(btnAjax_estilo);

	});

	//REMOVE CLASS FA > DEFAULT BOX POST
	$('.bar-formatPost').removeClass('fa');


	//ADD MENU MY LIBRARY
	let dshMenu 		= $('.woocommerce-MyAccount-navigation-link--orders');
	let dshAdm			= 'Hello';
	let dshLi 			= '<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--my-folders"><a id="cisziFolders" href="#">Mi Libreria</a></li>';
	let cisziFolders	= '../mi-libreria';
	
		$('.woocommerce-MyAccount-navigation-link--orders').prepend(dshLi);
		$('#cisziFolders').attr('href', "../mi-libreria");


	//CUSTOM FOOTER MENU
	$('.search').click(function () {
		 window.location = "./mi-libreria";
	})

	//CUSTOM HEADER POST
	let head_post_div = '<div id="head_post_div"></div>';
	let posted_on = $('.posted-on');
	let post_author = $('.author');

		$('.entry-header').append(head_post_div);
		$('#head_post_div').append(posted_on, post_author);

	//CUSTOM CARD POSTS
	let card_txt = $('.card-item-txt');
	let card_on = 'card-item-txt-on';
	let card_off = 'card-item-txt-off';

		$(card_txt).hover(function (){
			$(this).toggleClass(card_on);
			$(this).toggleClass(card_off);
		});

	//POST
	let author_link = $('.author > a');
	let cat_link = $('.cat-links > a');

		$(author_link).attr('href', '#');
		$(cat_link).attr('href', '#');

	//CUSTOM SINGUP
	let btn_mainRegister	= '<div id="btn_mainRegister"></div>'
	let btn_singIn 			= '<div class="btn_register btn_singIn"><h2>Acceder</h2></div>'
	let btn_singUp 			= '<div class="btn_register btn_singUp"><h2>Registrar</h2></div>'
	let box_singIn			= $('.u-column1.col-1')
	let box_singUp			= $('.u-column2.col-2')
	let box_class_on		= 'box_class_on'
	let box_class_off		= 'box_class_off'

		$('#customer_login').before(btn_mainRegister);
		$('#btn_mainRegister').append(btn_singIn, btn_singUp);
		
		$(box_singUp).addClass(box_class_off);

		$('.btn_singIn').click(function(){
			$(box_singIn)
				.addClass(box_class_on)
				.removeClass(box_class_off);
			$(box_singUp)
				.addClass(box_class_off)
				.removeClass(box_class_on);
		});

		$('.btn_singUp').click(function(){
			$(box_singIn)
				.addClass(box_class_off)
				.removeClass(box_class_on);
			$(box_singUp)
				.addClass(box_class_on)
				.removeClass(box_class_off);
		});


})