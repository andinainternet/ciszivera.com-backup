<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php include( plugin_dir_path( __FILE__ ). 'button_admin_saveto.php' ); ?>
	</div>
</div>
<hr />
<div class="row">
	<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
		<?php include( plugin_dir_path( __FILE__ ). 'sidebar_admin_folders.php' ); ?>
	</div>
	<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
		<?php include( plugin_dir_path( __FILE__ ). 'content_admin_folders.php' ); ?>
	</div>
</div>