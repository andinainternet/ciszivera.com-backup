<div class="ciszi-sidebar-admin">
	<?php if( $metafolders ) : ?>

	<input type="submit" class="btn btn-primary ciszi-admin-folders" value="Organiza tus Folders" />

	<ul class="ciszi-list-admin">
			
		<li class="active"><a href="" id="ciszi-all" data-folder="ciszi-all" class="ciszi-to-folder"><h4>Mostrar Todo</h4></a></li>
		<?php foreach( $metafolders['list_folders'] as $kfolder => $vfolder ) : ?>
			<li><a href="" id="<?php echo $kfolder; ?>" data-folder="<?php echo $kfolder; ?>" class="ciszi-to-folder"><?php echo $vfolder; ?></a></li>
		<?php endforeach; ?>

	</ul>

	<?php endif; ?>

	<a href="" class="ciszi-lnk-create-new btn btn-primary"><h4>Crear Folder</h4></a>
	<div class="ciszi-win-create-new" style="display:none;">
		<input type="text" name="ciszi-in-create-new" class="ciszi-in-create-new" placeholder="Nombre de la carpeta" /><br />
		<input type="submit" name="ciszi-btn-create-new" class="btn btn-primary" value="Crear Carpeta" />
	</div>
</div>